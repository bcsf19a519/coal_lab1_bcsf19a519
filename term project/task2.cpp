#include <iostream>
#include <fstream>
#include <bitset>
#include <algorithm>
#include <map>

//#include "Parser.cpp"
//#include "CodeTranslator.cpp"
//#include "SymbolTable.cpp"

using namespace std;

class Parser {
    ifstream fin;
    string currentCommand;
    map<char, char> commandTable;
public:
    Parser(string& fileName){
    fin.open(fileName);

    if (fin.fail()) {
        cout << fileName << " not found." << endl;
        exit(1);
    }

    // Populate the command map table.
    commandTable['@'] = 'A';
    commandTable['A'] = 'C';
    commandTable['D'] = 'C';
    commandTable['M'] = 'C';
    commandTable['0'] = 'C';
    commandTable['1'] = 'C';
    commandTable['-'] = 'C';
    commandTable['!'] = 'C';
    commandTable['('] = 'L';
}

bool hasMoreCommands() {
    return !fin.eof();
}

void advance(unsigned long& lineNr) {
    string currentLine;
    unsigned long commentPos;
    bool commandFound;

    commandFound = false;

    // Read lines until a command is found.
    while (!commandFound && getline(fin, currentLine)) {
        // Increment the line number corresponding to the source file,
        //  used for error tracking.
        lineNr++;

        // Remove whitespace.
        currentLine.erase(remove_if(currentLine.begin(), currentLine.end(), ::isspace), currentLine.end());

        // Remove comments.
        commentPos = currentLine.find("//");
        if (commentPos != string::npos) {
            currentLine.erase(commentPos, currentLine.length() - commentPos);
        }

        // If the remaining line is not empty, a command has been found.
        commandFound = !currentLine.empty();
    }

    currentCommand = currentLine;
}

char commandType(unsigned long& lineNr) {
    if (commandTable.find(currentCommand[0]) != commandTable.end()) {
        return commandTable[currentCommand[0]];
    }

    // If an invalid command is found, output an error message and line number.
    cout << "Invalid syntax at line: " << lineNr << endl;
    exit(1);
}

string symbol() {
    unsigned long openBracketPos, closeBracketPos;

    openBracketPos = currentCommand.find('(');
    closeBracketPos = currentCommand.find(')');

    // A-instruction: return everything after the '@' character.
    if (currentCommand[0] == '@') {
        return currentCommand.substr(1, currentCommand.length() - 1);
    }
    // L-instruction: return everything in between the '(' and ')' characters.
    else if (openBracketPos != string::npos && closeBracketPos != string::npos) {
        return currentCommand.substr(openBracketPos + 1, closeBracketPos - openBracketPos - 1);
    }

    // If the function was called in error, return a blank string.
    return "";
}

string destM() {
    unsigned long equalSignPos;

    equalSignPos = currentCommand.find('=');

    // Return everything before the '=' character.
    if (equalSignPos != string::npos) {
        return currentCommand.substr(0, equalSignPos);
    }

    // If no destination was specified, return a blank string.
    return "";
}

string compM() {
    unsigned long equalSignPos, semiColonPos;

    equalSignPos = currentCommand.find('=');
    semiColonPos = currentCommand.find(';');

    // Return the computation mnemonic based on three cases.
    if (equalSignPos != string::npos) {
        if (semiColonPos != string::npos) {
            // Case 1: dest = comp ; jump
            return currentCommand.substr(equalSignPos + 1, semiColonPos - equalSignPos - 1);
        }
        // Case 2: dest = comp
        return currentCommand.substr(equalSignPos + 1, currentCommand.length() - equalSignPos - 1);
    }
    else if (semiColonPos != string::npos) {
        // Case 3: comp ; jump
        return currentCommand.substr(0, semiColonPos);
    }

    // If no computation was specified, return a blank string.
    // This will result in an error message at the line number.
    // The error is handled by CodeTranslator.comp().
    return "";
}

string jumpM() {
    unsigned long semiColonPos;

    semiColonPos = currentCommand.find(';');

    // Return everything after the ';' character.
    if (semiColonPos != string::npos) {
        return currentCommand.substr(semiColonPos + 1, currentCommand.length() - semiColonPos - 1);
    }

    // If a jump was not specified, return a blank string.
    return "";
}
};

class CodeTranslator {
    map<string, string> destTable;
    map<string, string> compTable;
    map<string, string> jumpTable;
public:
    CodeTranslator() {
    // Populate the translation map tables.
    destTable[""] = "000";
    destTable["M"] = "001";
    destTable["D"] = "010";
    destTable["MD"] = "011";
    destTable["A"] = "100";
    destTable["AM"] = "101";
    destTable["AD"] = "110";
    destTable["AMD"] = "111";

    compTable["0"] = "0101010";
    compTable["1"] = "0111111";
    compTable["-1"] = "0111010";
    compTable["D"] = "0001100";
    compTable["A"] = "0110000";
    compTable["!D"] = "0001101";
    compTable["!A"] = "0110001";
    compTable["-D"] = "0001111";
    compTable["-A"] = "0110011";
    compTable["D+1"] = "0011111";
    compTable["A+1"] = "0110111";
    compTable["D-1"] = "0001110";
    compTable["A-1"] = "0110010";
    compTable["D+A"] = "0000010";
    compTable["D-A"] = "0010011";
    compTable["A-D"] = "0000111";
    compTable["D&A"] = "0000000";
    compTable["D|A"] = "0010101";
    compTable["M"] = "1110000";
    compTable["!M"] = "1110001";
    compTable["-M"] = "1110011";
    compTable["M+1"] = "1110111";
    compTable["M-1"] = "1110010";
    compTable["D+M"] = "1000010";
    compTable["D-M"] = "1010011";
    compTable["M-D"] = "1000111";
    compTable["D&M"] = "1000000";
    compTable["D|M"] = "1010101";

    jumpTable[""] = "000";
    jumpTable["JGT"] = "001";
    jumpTable["JEQ"] = "010";
    jumpTable["JGE"] = "011";
    jumpTable["JLT"] = "100";
    jumpTable["JNE"] = "101";
    jumpTable["JLE"] = "110";
    jumpTable["JMP"] = "111";
}
    // Populates the code translation map tables
    //  with the language specification.

    string dest(string destMnemonic, unsigned long& lineNr) {
        if (destTable.find(destMnemonic) != destTable.end()) {
            return destTable[destMnemonic];
    }

    // If none of the mnemonics are found, output an error message,
    //  and provide the line number in the original source where the error occurred.
        cout << "Invalid syntax in destination statement at line: " << lineNr << endl;
        exit(1);
    }

    // Returns the binary code of the destination mnemonic
    //  as a string containing 3 bits.
    // Line number is passed for use in an error message.

    string comp(string compMnemonic, unsigned long& lineNr) {
        if (compTable.find(compMnemonic) != compTable.end()) {
            return compTable[compMnemonic];
        }

    // If none of the mnemonics are found, output an error message,
    //  and provide the line number in the original source where the error occurred.
        cout << "Invalid syntax in computation statement at line: " << lineNr << endl;
        exit(1);
    }

    // Returns the binary code of the computation mnemonic
    //  as a string containing 7 bits.
    // Line number is passed for use in an error message.

    string jump(string jumpMnemonic, unsigned long& lineNr) {
        if (jumpTable.find(jumpMnemonic) != jumpTable.end()) {
            return jumpTable[jumpMnemonic];
    }


    // If none of the mnemonics are found, output an error message,
    //  and provide the line number in the original source where the error occurred.
        cout << "Invalid syntax in jump statement at line: " << lineNr << endl;
        exit(1);
    }
    // Returns the binary code of the jump mnemonic
    //  as a string containing 3 bits.
    // Line number is passed for use in an error message.

};

class SymbolTable {
    map<string, int> symbolAddressTable;
public:
    SymbolTable() {
    // Populate the table with predefined symbols.
    symbolAddressTable["SP"] = 0;
    symbolAddressTable["LCL"] = 1;
    symbolAddressTable["ARG"] = 2;
    symbolAddressTable["THIS"] = 3;
    symbolAddressTable["THAT"] = 4;
    symbolAddressTable["R0"] = 0;
    symbolAddressTable["R1"] = 1;
    symbolAddressTable["R2"] = 2;
    symbolAddressTable["R3"] = 3;
    symbolAddressTable["R4"] = 4;
    symbolAddressTable["R5"] = 5;
    symbolAddressTable["R6"] = 6;
    symbolAddressTable["R7"] = 7;
    symbolAddressTable["R8"] = 8;
    symbolAddressTable["R9"] = 9;
    symbolAddressTable["R10"] = 10;
    symbolAddressTable["R11"] = 11;
    symbolAddressTable["R12"] = 12;
    symbolAddressTable["R13"] = 13;
    symbolAddressTable["R14"] = 14;
    symbolAddressTable["R15"] = 15;
    symbolAddressTable["SCREEN"] = 16384;
    symbolAddressTable["KBD"] = 24576;
}

void addEntry(string symbol, int address) {
    if (symbolAddressTable.find(symbol) == symbolAddressTable.end()) {
        symbolAddressTable[symbol] = address;
    }
}

bool contains(string symbol) {
    return (symbolAddressTable.find(symbol) != symbolAddressTable.end());
}

int getAddress(string symbol) {
    if (symbolAddressTable.find(symbol) != symbolAddressTable.end()) {
        return symbolAddressTable[symbol];
    }

    // If the table does not contain the symbol.
    return 0;
}
};

int main(int argc, char *argv[]) {
    string inputFileName, outputFileName;
    int lineNumberROM, newAddress;
    unsigned long lineNumberSource;
    ofstream fout;

    // Get the input and output file names, and provide usage instructions
    //  if too few or too many arguments are provided.
    if (argc < 2 || argc > 3) {
        cout << "Usage: " << argv[0] << " <inputfilename.asm> <(optional) outputfilename.hack>" << endl;
        exit(1);
    }
    else {
        inputFileName = argv[1];
        outputFileName = inputFileName.substr(0, inputFileName.length() - 4) + ".hack";

        if (argc == 3) {
            outputFileName = argv[2];
        }
    }

    // Create the output file
    fout.open(outputFileName);

    if (fout.fail()) {
        cout << "Failed to create output file." << endl;
        exit(1);
    }

    /*
     * First pass: Generate symbol table
     *  This is done by reading the input file line by line using the symbolSource parser,
     *  and looking for pseudo instructions. If an A-instruction or C-instruction is encountered,
     *  lineNumberROM is incremented, which ultimately corresponds to the program counter address
     *  of the instructions in the ROM / .hack output file. If an L-instruction is encountered
     *  and it does not already exist in the symbol table, the symbol and the current
     *  program counter address is stored in the symbol table.
     */

    Parser symbolSource(inputFileName);
    SymbolTable symbolTable;

    lineNumberSource = 0;
    lineNumberROM = 0;

    while (true) {
        symbolSource.advance(lineNumberSource);

        if (!symbolSource.hasMoreCommands()) {
            break;
        }

        if (symbolSource.commandType(lineNumberSource) == 'A' || symbolSource.commandType(lineNumberSource) == 'C') {
            lineNumberROM++;
        }

        if (symbolSource.commandType(lineNumberSource) == 'L' && !symbolTable.contains(symbolSource.symbol())) {
            symbolTable.addEntry(symbolSource.symbol(), lineNumberROM);
        }
    }

    /*
     * Second pass: Assemble machine code
     *  With the symbol table generated, we read the file line by line once more,
     *  this time using the assemblySource parser. Since the L-instructions have been dealt with,
     *  we are only looking for A-instructions and C-instructions.
     *
     *  If an A-instruction is encountered, the address can either be a number,
     *  a predefined symbol, or a user defined variable. If the address is a number,
     *  the string is converted to its numeric representation in decimal,
     *  which is then converted to binary and output to the file.
     *  If the address is a symbol, the corresponding address is either
     *  retrieved if it is a predefined symbol, or generated if it is a user defined variable,
     *  and this address is then converted into binary and output to the file.
     *
     *  If a C-instruction is encountered, the destination, computation and jump mnemonics
     *  are converted into binary code using the translator, and the resulting bit string
     *  is output to the file.
     */

    Parser assemblySource(inputFileName);
    CodeTranslator translator;

    lineNumberSource = 0;   // Reset the line number for the error handling.
    newAddress = 16;        // Predefined symbols occupy addresses 0-15.

    while (true) {
        assemblySource.advance(lineNumberSource);

        if (!assemblySource.hasMoreCommands()) {
            break;
        }

        if (assemblySource.commandType(lineNumberSource) == 'A') {
            fout << '0';    // A-instructions always start with '0'.

            // Check if the symbol is a number.
            if (assemblySource.symbol().find_first_not_of("0123456789") == string::npos) {
                // Convert the string to a decimal number, convert the decimal number to a binary number.
                fout << bitset<15>(stoull(assemblySource.symbol(), nullptr)).to_string();
            }
            else {
                // Check if the symbol is a variable.
                if (!symbolTable.contains(assemblySource.symbol())) {
                    symbolTable.addEntry(assemblySource.symbol(), newAddress++);
                }
                // Retrieve the address, and convert the decimal number to a binary number.
                fout << bitset<15>(static_cast<unsigned long long>(symbolTable.getAddress(assemblySource.symbol()))).to_string();
            }
            fout << endl;
        }
        else if (assemblySource.commandType(lineNumberSource) == 'C') {
            fout << "111";  // C-instructions always start with "111".
            fout << translator.comp(assemblySource.compM(), lineNumberSource);
            fout << translator.dest(assemblySource.destM(), lineNumberSource);
            fout << translator.jump(assemblySource.jumpM(), lineNumberSource);
            fout << endl;
        }
    }

    fout.close();
    return 0;
}
//source https://github.com/francoiswnel/Hack-Assembler
