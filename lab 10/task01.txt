1-The two ways of passing command line arguments to GDB are
->(gdb) run arg1 arg2 arg 3...
->(gdb) Set args arg1 argè arg3...

2-We do it by following command
->(gdb) break <line#> if <condition>
for example: break 7 if c==99

3-We do it by the following command
->(gdb) step/s/si

4-We do it by the following command
->(gdb) step/s/si

5-we do it by following command
->(gdb) continue/c/ci

6-We do it by following command
->(gdb) continue/c/ci

7-We do it by following command
->(gdb) print / format-char <var-name>

8-We do it by following command
->(gdb) display <var-name>

9-We do it by the following command
->(gdb) info variables